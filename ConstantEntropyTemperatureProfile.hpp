#include "EquationsOfState/EOS.hpp"
#include "Utilities/Interpolators/PiecewiseLinearFunction.hpp"
#include "NuclearData/NuclideLibrary.hpp" 

/*
  Given an entropy, a starting time, an ending time, a composition, a density profile, and an EOS, calculate 
  the corresponding temperature vs. time profile. 

  Example of use for constant entropy (assuming fixed composition) evolution (assuming standard variables in r-process.cpp): 
   
    auto temperatureProfile = GetTemperatureProfileFromFixedEntropy(s, tstart, 
        tfinal, nseResult.Y(), densityProfile, &helm, nuclib);
    
    auto output = net.Evolve(nseResult.Y(), tstart, tfinal, &temperatureProfile, 
        &densityProfile, runIdent); 

*/
PiecewiseLinearFunction GetTemperatureProfileFromFixedEntropy(double s, double tstart, double tfinal, 
    const std::vector<double>& Y, const FunctionVsTime<double>& densityProf, EOS* eos, const NuclideLibrary& nucLib) { 
   
  std::vector<double> ttimes; 
  std::vector<double> T9; 
  double t = tstart;
  double Tlast = 10.0; // Start with an arbitrary value, hopefully there is no error to start 
  
  while (t <= tfinal*1.1) { 
    // Calculate the temperature given a fixed composition and entropy, but varying density 
    try {
      auto state = eos->FromSAndRho(s, densityProf(t), Y, nucLib);   
      T9.push_back(state.T9());
      Tlast = state.T9();
    } catch (std::runtime_error& a) { 
      // Probably got to too low of a density for the EOS, and at this point the temperature probably 
      // doesn't matter for the network evolution, so just keep T fixed
      // Usually the EOS complains about this so there is a lot of output to screen
      T9.push_back(Tlast);
    } 
    
    // Add this time to our array of times 
    ttimes.push_back(t); 
    
    // Update the time by one density e-folding timescale
    double delta = 1.e-5;
    double dt = fabs(delta*t*densityProf(t)/(densityProf(t*(1.0+delta))-densityProf(t*(1.0-delta))));
    t = t + dt; 
  }

  return PiecewiseLinearFunction(ttimes, T9, true); 
}
