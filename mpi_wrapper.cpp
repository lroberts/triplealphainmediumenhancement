/// MPI wrapper testing 
/// 
///


#include <string>
#include <vector> 
#include <algorithm> 
#include <map> 
#include <iostream>
#include <mpi.h> 
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>


#include "RunSetup.hpp" 

std::string double2string(double dnum){
  std::string str1;
  std::stringstream ss1;
  ss1<<dnum;
  ss1>>str1;
  return str1;
}

int main(int nargin, char** args) {
  
  // Perform standard MPI initialization 
  int world_rank, world_size;
  MPI_Init(NULL, NULL); 
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank); 
  MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
   
  // Message tags  
  const int request_message_tag = 1;
  const int num_message_tag = 2;
  const int s_message_tag = 5;
  const int tau_message_tag = 6;
  const int ye_message_tag = 7;

   
  std::string baseDir =  "./";
  if (nargin>1) baseDir = std::string(args[1]); 
  
  std::string finishedDir =  "./";
  if (nargin>2) finishedDir = std::string(args[2]); 
  
  std::string basename =  "enhanced";
  std::string plus =  "_";
  std::string filename;
  
  //Master Process
  if (world_rank == 0) { 
    double yec,sc,tauc;
    std::vector<double> Yes; 
    std::vector<double> Ss; 
    std::vector<double> taus; 
    std::ifstream gasinmul;
    std::string  inmulname ="./grid_Ye_S_tau.txt";
    gasinmul.open(inmulname.data());
    int z=0;
    while(gasinmul.good())
      {
    	gasinmul>>yec>>sc>>tauc;
    	if (!gasinmul.good()) break;
      Yes.push_back(yec);
    	Ss.push_back(sc);
    	taus.push_back(tauc);
    	z++;
      }
    gasinmul.close();
    
    for (unsigned int i=0; i<Yes.size(); ++i) { 
    // for (int i=0; i<10; ++i) { 
    // for (int i=0; i<4; ++i) { 
      // Receive a job request from a slave process
      int rank; // rank of the requesting process
      MPI_Status status;
      MPI_Recv(&rank, 1, MPI_INT, MPI_ANY_SOURCE, request_message_tag, MPI_COMM_WORLD, &status); 

      // Send data to the process that has asked for it  
      MPI_Send(&i, 1, MPI_INT, rank, num_message_tag, MPI_COMM_WORLD);
      //MPI_Send_string(rank, data_message_tag, MPI_COMM_WORLD, file);
      double ye = Yes[i];
      double tau = taus[i];
      double s = Ss[i];
      MPI_Send(&ye, 1, MPI_DOUBLE, rank, ye_message_tag, MPI_COMM_WORLD);
      MPI_Send(&tau, 1, MPI_DOUBLE, rank, tau_message_tag, MPI_COMM_WORLD);
      MPI_Send(&s, 1, MPI_DOUBLE, rank, s_message_tag, MPI_COMM_WORLD);
      // Send parameters   
      std::cout << "Send   Parameter: " << ye <<"  "<<tau<<"  "<<s<< " Rank:" <<world_rank<< " Num: "<<i<< std::endl;  
    }

    // Send a message to all other processes indicating that all calculations are done
    for (int i=0; i<world_size; ++i) {
      int done_indicator = -1;  
      MPI_Send(&done_indicator, 1, MPI_INT, i, num_message_tag, MPI_COMM_WORLD);
    }
    // Slave process
  } else {
    
    RunSetup netRunnerBase("temp", false, 0.5,  5.0, 10.0); 
    netRunnerBase.opts.DisableStdoutOutput = true;
    
    RunSetup netRunnerEnhanced("temp", true, 0.5, 5.0, 10.0); 
    netRunnerEnhanced.opts.DisableStdoutOutput = true;
    
    int number = 0;
    do {
      // Ask for some data from the main process, use a non-blocking send since we don't want 
      // to wait for the main process to receive it if we are done with the calculations 
      MPI_Request my_request;  // Never used because we are not worried about wether or 
                               // not rank 0 received our message as long as we get data
      MPI_Isend(&world_rank, 1, MPI_INT, 0, request_message_tag, MPI_COMM_WORLD, &my_request); 

      // Check if there are more calculations to be done
      MPI_Status status;
      MPI_Recv(&number, 1, MPI_INT, 0, num_message_tag, MPI_COMM_WORLD, &status); 
      if (number < 0) break; // Leave the loop if there is nothing left 

      double ye, tau, s;
      MPI_Recv(&ye, 1, MPI_DOUBLE, 0, ye_message_tag, MPI_COMM_WORLD, &status); 
      MPI_Recv(&tau, 1, MPI_DOUBLE, 0, tau_message_tag, MPI_COMM_WORLD, &status); 
      MPI_Recv(&s, 1, MPI_DOUBLE, 0, s_message_tag, MPI_COMM_WORLD, &status); 
      std::cout << "Recv   Parameter: " << ye <<"  "<<tau<<"  "<<s<< " Rank:" <<world_rank<< " Num:"<<number<<std::endl;  
      std::string Sye,Stau,StS;
      Sye=double2string(ye*100);
      Stau=double2string(tau);
      StS=double2string(s*1);
      
      // Run the base case
      filename="lowbe9base_nu"+plus+Sye+plus+Stau+plus+StS;
      std::cout<<filename<<std::endl;
      try {
        netRunnerBase.RunRprocess(filename,ye,tau,s, 1.e9, true);
      } catch(...) { 
        std::cout << "Some error occurred for run " << filename << std::endl;
      }
      
      // Run the base case
      filename="base"+plus+Sye+plus+Stau+plus+StS;
      std::cout<<filename<<std::endl;
      try {
        netRunnerBase.RunRprocess(filename,ye,tau,s, 1.e9, false);
      } catch(...) { 
        std::cout << "Some error occurred for run " << filename << std::endl;
      }

      // Run the enhanced case
      filename="lowbe9enhancedHi_nu"+plus+Sye+plus+Stau+plus+StS;
      std::cout<<filename<<std::endl;
      try {
        netRunnerEnhanced.RunRprocess(filename,ye,tau,s, 1.e9, true);
      } catch(...) { 
        std::cout << "Some error occurred for run " << filename << std::endl;
      }
      
      // Run the enhanced case without neutrinos
      filename="enhanced"+plus+Sye+plus+Stau+plus+StS;
      std::cout<<filename<<std::endl;
      try {
        netRunnerEnhanced.RunRprocess(filename,ye,tau,s, 1.e9, false);
      } catch(...) { 
        std::cout << "Some error occurred for run " << filename << std::endl;
      }

      

    } while (number >= 0);
  
  }  
  
  MPI_Finalize(); 
  return 0; 
}



