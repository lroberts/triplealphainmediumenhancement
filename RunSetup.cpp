 
/// \file base_run.cpp
/// \author lroberts
/// \since Jun 7, 2018
///
/// \brief
///
///
#include <string>
#include <vector> 
#include <algorithm> 
#include <map> 
#include <sstream> 

#include "RunSetup.hpp" 
#include "ConstantEntropyTemperatureProfile.hpp" 
#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "EquationsOfState/SkyNetScreening.hpp"
#include "Network/ReactionNetwork.hpp"
#include "Network/NSE.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
#include "DensityProfiles/PowerLawContinuation.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
//#include "MultiplierReactionLibrary.hpp" 
#include "NuclearData/Nuclide.hpp"
#include "Utilities/Interpolators/PiecewiseLinearFunction.hpp"
#include "Utilities/Constants.hpp"
#include "Reactions/SpecialReactionLibrary.hpp"
#include "Reactions/NeutrinoReactionLibrary.hpp"
#include "AlphaBurning.hpp"
#include "WindProfile.hpp"
#include "EquationsOfState/NeutrinoHistoryBlackBody.hpp" 

RunSetup::~RunSetup() { 
  for (auto& lib : reactionLibs) delete lib;  
} 
 
RunSetup::RunSetup(std::string runName, bool enhanced, double be9alphanEnhance,
    double enhancePEnhance, double enhanceNEnhance) : 
      runName(runName),
      nuclib(NuclideLibrary::CreateFromWebnucleoXML(SkyNetRoot + "/data/webnucleo_nuc_v2.0.xml"
          )),
          //,std::vector<std::string>({"n", "p", "d", "t", "he3", "he4", "be9", "c12"}))),
          //,std::vector<std::string>({"n", "p"}))),
      eos(HelmholtzEOS(SkyNetRoot + "/data/helm_table.dat"))
{ 
  opts.ConvergenceCriterion = NetworkConvergenceCriterion::DeltaYByY;
  opts.MassDeviationThreshold = 1.0E-10;
  opts.DeltaYByYThreshold = 1.e-5;
  //opts.ConvergenceCriterion = NetworkConvergenceCriterion::Mass;
    
  opts.IsSelfHeating = true;
  opts.EnableScreening = true;
  
  opts.NSEEvolutionMinT9 = 9.0; 
  opts.NetworkEvolutionMaxT9 = 9.3; 

  LoadREACLIBLibraries(enhanced, be9alphanEnhance, enhancePEnhance, enhanceNEnhance);
} 

void RunSetup::LoadREACLIBLibraries(bool enhanced, double be9alphanEnhance, 
    double enhancePEnhance, double enhanceNEnhance) { 
    
  ReactionLibraryBase* strongReacLib
      = new REACLIBReactionLibrary(SkyNetRoot + "/data/reaclib",
        ReactionType::Strong, true, LeptonMode::TreatAllAsDecayExceptLabelEC,
        "Strong reactions", nuclib, opts, false);
  reactionLibs.push_back(strongReacLib); 

  const ReactionLibraryBase* symFis
      = new REACLIBReactionLibrary(SkyNetRoot + 
        "/data/netsu_panov_symmetric_0neut", ReactionType::Strong, false,
        LeptonMode::TreatAllAsDecayExceptLabelEC,
        "Symmetric neutron induced fission with 0 free neutrons", nuclib, opts,
        false);
  reactionLibs.push_back(symFis); 

  const ReactionLibraryBase* spontFis 
      = new REACLIBReactionLibrary(SkyNetRoot + 
        "/data/netsu_sfis_Roberts2010rates", ReactionType::Strong, false,
        LeptonMode::TreatAllAsDecayExceptLabelEC, "Spontaneous fission", nuclib,
        opts, false);
  reactionLibs.push_back(spontFis); 

  
  // use only REACLIB weak rates
  const ReactionLibraryBase* weakLib
      = new REACLIBReactionLibrary(SkyNetRoot + "/data/reaclib",
      ReactionType::Weak, false, LeptonMode::TreatAllAsDecayExceptLabelEC,
      "Weak reactions", nuclib, opts, false);
  reactionLibs.push_back(weakLib); 
  
  // Check that required nuclei are included in network
  if (nuclib.NuclideIdsVsNames().count("he4") 
      && nuclib.NuclideIdsVsNames().count("c12") 
      && nuclib.NuclideIdsVsNames().count("n") 
      && nuclib.NuclideIdsVsNames().count("p")) {  
    // Create alpha particle reaction library 
    const ReactionLibraryBase* alphaLib 
        = new SpecialReactionLibrary(AlphaBurning::GetRates(nuclib, enhanced, 
            be9alphanEnhance, enhancePEnhance, enhanceNEnhance), 
        ReactionType::Strong, "test", "Local", nuclib, opts); 
    reactionLibs.push_back(alphaLib); 
    
    // Remove special reactions from the base REACLIB library to avoid double counting
    strongReacLib->RemoveReactions(alphaLib->Reactions().AllData()); 
  }
  
  // Add neutrino reactions 
  const ReactionLibraryBase* nulib = new NeutrinoReactionLibrary(
      SkyNetRoot + "/data/neutrino_reactions.dat", 
      "weak capture rates", nuclib, opts); 
  reactionLibs.push_back(nulib); 

}

int RunSetup::RunRprocess(std::string filename, double Ye, double tau, 
    double s, double tend, bool useNeutrinos) {
  
  SkyNetScreening screen(nuclib); 
  ReactionNetwork net(nuclib, reactionLibs, &eos, &screen, opts); 

  NSE nse(net.GetNuclideLibrary(), &eos, &screen); 
  
  double tstart = 0.0;
  double T0 = 10.0;
  
  auto nseResult = nse.CalcFromTemperatureAndEntropy(T0, s, Ye);
   
  //auto densityProfile = ExpTMinus3(nseResult.Rho(), tau / 1000.0);
  
  // Use a model with a wind termination shock 
  double r0 = 5.e6; 
  double rwt = 2.e7;
  double shockJump = 3.0;
  std::vector<double> nuTimes, nuRadii;
  auto densityProfile = GetWindProfile(nseResult.Rho(), r0, tau/1000.0, rwt, 
      shockJump, &nuTimes, &nuRadii);
  
  // Create a neutrino history 
  if (useNeutrinos) {
    double T9nue = 48.04; // 13 MeV
    double T9nub = 51.74; // 14 MeV 
    double Lnue = 1.2e52;
    double Lnub = 1e52;
    auto nuHist = NeutrinoHistoryBlackBody::CreateConstant( nuTimes,
      nuRadii, { NeutrinoSpecies::NuE, NeutrinoSpecies::AntiNuE },
      {T9nue, T9nub} , {0.0, 0.0} , {Lnue, Lnub}, false);
    nuHist.PointSource(true); // Do not use the BB radius fix 
    
    // Add the Neutrinos to the network 
    net.LoadNeutrinoHistory(nuHist.MakeSharedPtr());
  }

  auto output = net.EvolveSelfHeatingWithInitialTemperature(nseResult.Y(), 
      tstart, tend, T0, &densityProfile, filename);

  return 0;
  
}







