/// \file windProfile.hpp
/// \author lroberts
/// \since Feb 1, 2020
///
/// \brief
///
///

#ifndef WINDPROFILE_HPP_
#define WINDPROFILE_HPP_

#include <stdio.h> 
#include <math.h> 

#include "DensityProfiles/ExpTMinus3.hpp"
#include "DensityProfiles/PowerLawContinuation.hpp"

PowerLawContinuation GetWindProfile(double rho0, double r0, double taud, 
    double rwt, double shockJump, 
    std::vector<double>* times,
    std::vector<double>* r,
    double alpha1 = 4.0/3.0, double alpha2 = 1.0) {
    
  int npoints = 10000; 
  std::vector<double> rho(npoints); 
  *r = std::vector<double>(npoints); 
  *times = std::vector<double>(npoints); 
  
  // Various constants for pre-shock profile 
  //double alpha1 = 4.0/3.0; // goes to rho \propto t^-3 
  //double tt1 = (3.0*alpha1 - 1.0)*taud; // Continuous rho derivative
  double tt1 = (3.0*alpha1)*taud; 
  double rt1 = r0*exp(tt1/(3.0*taud)); 
  double rhot1 = rho0*exp(-tt1/taud);  
  
  // Find the time of the shock  
  double twt, rhowt, vwt, tauwt;  
  if (rwt > rt1) {
    twt = tt1*pow(rwt/rt1, 1.0/alpha1);
    vwt = rt1/tt1*pow(twt/tt1, alpha1 - 1.0)/shockJump; 
    tauwt = rwt/(3.0*vwt); 
    rhowt = shockJump*rhot1*pow(twt/tt1, 1.0 - 3.0*alpha1);
  } else {
    twt = 3.0*taud*log(rwt/r0);
    vwt = r0/(3.0*taud)*exp(-twt/(3.0*taud))/shockJump; 
    tauwt = rwt/(3.0*vwt); 
    rhowt = shockJump*rho0*exp(-twt/taud);  
  }

  // Constants for post shock region (time is relative to twt) 
  //double alpha2 = 1.0; // goes to rho \propto t^-2
  //double tt2 = (3.0*alpha2 - 1.0)*tauwt; 
  double tt2 = (3.0*alpha2)*tauwt; 
  double rt2 = rwt*exp(tt2/(3.0*tauwt)); 
  double rhot2 = rhowt*exp(-tt2/tauwt);  
 
  double log10tMin = log10(1.e-5); 
  double log10tMax = log10(1.e10);
  printf("# %f \n", tauwt); 
    
  (*times)[0] = 0.0;
  rho[0] = rho0;
  (*r)[0] = r0;
  for (int i=1; i<npoints; ++i) { 
    double tc = pow(10.0, log10tMin + (log10tMax - log10tMin)*(double) (i-1)/ (double) (npoints-2));  
    (*times)[i] = tc;
    // Initial exponential fall off 
    if (tc<tt1) {
      rho[i] = rho0*exp(-tc/taud); 
      (*r)[i] = r0*exp(tc/(3.0*taud)); 
    }
    // Power law expansion 
    else if (tc<twt) {
      rho[i] = rhot1*pow(tc/tt1, 1.0 - 3.0*alpha1);
      (*r)[i] = rt1*pow(tc/tt1, alpha1); 
    }
    // Jump to shock and then fall off exponentially  
    else if (tc<twt+tt2) {
      rho[i] = rhowt*exp(-(tc - twt)/tauwt); 
      (*r)[i] = rwt*exp((tc - twt)/(3.0*tauwt)); 
    }
    // Transition to constant velocity wind profile
    else {
      rho[i] = rhot2*pow((tc-twt)/tt2, 1.0 - 3.0*alpha2); 
      (*r)[i] = rt2*pow((tc-twt)/tt2,alpha2);
    }
    //printf("%e %e %e \n", times[i], r[i], rho[i]);
  }
   
  PiecewiseLinearFunction pieceWiseRho(*times, rho, true);
  return PowerLawContinuation(pieceWiseRho, 1.0 - 3.0*alpha2, pow(10.0, log10tMax)*0.05);
}

#endif 
