#ifndef RUNSETUP
#define RUNSETUP 

#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
//#include "EquationsOfState/SkyNetScreening.hpp"
#include "Network/ReactionNetwork.hpp"
#include "Network/NSE.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
//#include "MultiplierReactionLibrary.hpp" 
#include "NuclearData/Nuclide.hpp"
#include "Utilities/Interpolators/PiecewiseLinearFunction.hpp"
//#include "misc.hpp"


class RunSetup {
public: 
  RunSetup(std::string runName = "defaultRunName", bool enhanced = false, 
      double be9alphanEnhance = 1.0, double enhancePEnhance = 1.0, 
      double enhanceNEnhance = 1.0);
  ~RunSetup();
  
  NetworkOptions opts;
  std::string runName;

  int RunRprocess(std::string fname, double Ye, double tau, double s, 
      double tend, bool useNeutrinos = false); 

private:
  NuclideLibrary nuclib;
  HelmholtzEOS eos;
  
  std::vector<const ReactionLibraryBase*> reactionLibs; 
  void LoadREACLIBLibraries(bool enhanced, double be9alphanEnhance, 
    double enhancePEnhance, double enhanceNEnhance); 
  void ReadProfiles(std::string fname, PiecewiseLinearFunction* rhoVsTime, 
    PiecewiseLinearFunction* T9VsTime, PiecewiseLinearFunction* yeVsTime);
  void ReadProfiles(std::string fname, std::vector<double>* time, 
    std::vector<double>* rho, std::vector<double>* T9, std::vector<double>* ye);
};

#endif 
