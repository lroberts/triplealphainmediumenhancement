/// \file r-process.cpp
/// \author jlippuner
/// \since Sep 3, 2014
///
/// \brief
///
///

#include <math.h>
#include <string> 
 
#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "EquationsOfState/NeutrinoHistory.hpp"
#include "EquationsOfState/Screening.hpp"
#include "Network/ReactionNetwork.hpp"
#include "Network/NSE.hpp"
#include "Network/NetworkOutput.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
#include "Reactions/SpecialReactionLibrary.hpp"
#include "Reactions/ReactionPostProcess.hpp" 
#include "Utilities/FunctionVsTime.hpp" 
#include "EquationsOfState/SkyNetScreening.hpp"
#include "AlphaBurning.hpp" 

int main(int, char** command) {
  
  std::string runIdent = command[1];
  std::cout << "Analyzing " << runIdent << "..." <<  std::endl; 
  
  HelmholtzEOS eos(SkyNetRoot + "/data/helm_table.dat");
  
  
  auto nuclib = NuclideLibrary::CreateFromWebnucleoXML(
      SkyNetRoot + "/data/webnucleo_nuc_v2.0.xml");
  NetworkOptions opts;
  
  SkyNetScreening screen(nuclib); 
  
  SpecialReactionLibrary alphaLib(AlphaBurning::GetRates(nuclib, true, 1.0, 1.0, 1.0), 
      ReactionType::Strong, "test", "Local", nuclib, opts); 
  
  auto output = NetworkOutput::ReadFromFile(runIdent);  

  std::vector<double> finalYVsA = output.FinalYVsA();

  ReactionPostProcess postProcess(&alphaLib, &screen, nuclib, runIdent);
  auto alphaProd = postProcess.GetNucleusProductionRates("he4"); 
  auto alphaDest = postProcess.GetNucleusDestructionRates("he4"); 
  auto times = postProcess.GetTimes(); 
  int neutIdx = nuclib.NuclideIdsVsNames().at("n"); 
  int protIdx = nuclib.NuclideIdsVsNames().at("p"); 
  int alphaIdx = nuclib.NuclideIdsVsNames().at("he4"); 
  int c12Idx = nuclib.NuclideIdsVsNames().at("c12"); 
   
  
  
  std::vector<int> As = output.A(); 
  
  { 
    FILE * f = fopen((runIdent + ".final_y").c_str(), "w");
    for (unsigned int A = 0; A < finalYVsA.size(); ++A)
      fprintf(f, "%6i  %30.20E\n", A, finalYVsA[A]);
  }
  
  {
    FILE * f = fopen((runIdent + ".evolution").c_str(), "w");
    fprintf(f, "#[ 1] Time (s) \n");
    fprintf(f, "#[ 2] Density (g/cc) \n");
    fprintf(f, "#[ 3] Temperature (GK) \n");
    fprintf(f, "#[ 4] Y_n \n");
    fprintf(f, "#[ 5] Y_p \n");
    fprintf(f, "#[ 6] Y_alpha \n");
    fprintf(f, "#[ 7] Y_C12 \n");
    fprintf(f, "#[ 8] Y_{A>=12} \n");
    int idx = 9;
    for (unsigned int i=0; i < alphaDest.size(); ++i) 
      fprintf(f, ("#[%2i] " + alphaDest[i].reac.String() + "\n").c_str(), idx + i);
    idx += alphaDest.size(); 
    for (unsigned int i=0; i < alphaProd.size(); ++i) 
      fprintf(f, ("#[%2i] " + alphaProd[i].reac.String() + "\n").c_str(), idx + i);

    for (unsigned int i = 0; i<times.size(); ++i) {
      double Yseed = 0.0; 
      for (unsigned int j = 0; j<As.size(); ++j)
        if (As[j]>=12)  Yseed = Yseed+output.YVsTime()[i][j];
	

      fprintf(f, "%10.3E %10.3E %10.3E %10.3E %10.3E %10.3E %10.3E %10.3E", 
          times[i], 
          output.DensityVsTime()[i],
          output.TemperatureVsTime()[i],
          output.YVsTime()[i][neutIdx], 
          output.YVsTime()[i][protIdx], 
          output.YVsTime()[i][alphaIdx], 
          output.YVsTime()[i][c12Idx], 
          Yseed);
      
      
      for (auto& flux : alphaDest) 
        fprintf(f, " %10.3E", flux.rate[i]);  
      for (auto& flux : alphaProd) 
        fprintf(f, " %10.3E", flux.rate[i]);  
      fprintf(f, "\n"); 
    
    }
  }

  // Write final p-nuclide abundances out to file 
  std::vector<std::string> pNuclides = {"se74", "kr78", "sr84", "mo92", "ru96", "pd102", 
    "cd106", "sn112", "te120", "xe124", "xe126"};
  std::vector<int> pNuclideIdxs;
  for (auto pNuclide : pNuclides) { 
    int idx = nuclib.NuclideIdsVsNames().at(pNuclide);
    // Need to cross compare since nuclide library idxs do not 
    // correspond to NetworkOutput idxs 
    for (std::size_t i; i<output.A().size(); ++i) {
      if ((output.A()[i] == nuclib.As()[idx]) && (output.Z()[i] == nuclib.Zs()[idx])) {
        pNuclideIdxs.push_back(i);
        break;
      }
    }
  }
  
  {
    FILE * f = fopen((runIdent + ".nuclideAbundances").c_str(), "w");
    fprintf(f,"#[1] A \n");
    fprintf(f,"#[2] Z \n");
    fprintf(f,"#[3] YFinal \n");
    for (auto nuc : pNuclides) 
      fprintf(f,("# " + nuc + "\n").c_str());
    for (int i : pNuclideIdxs) {
      fprintf(f, "%3i %3i %30.20E \n", output.A()[i], output.Z()[i], output.FinalY()[i]);
    }
  }
}

